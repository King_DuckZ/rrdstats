#ifndef idAC920ADE4F224C25B11DB4D9C2AE0389
#define idAC920ADE4F224C25B11DB4D9C2AE0389

#include <boost/iterator/iterator_facade.hpp>

namespace stats {
	template <typename T, typename S, typename ValGet, typename Upgrade>
	class FillingIterator : public boost::iterator_facade<FillingIterator<T, S, ValGet, Upgrade>, typename T::value_type, typename T::iterator_category, typename T::value_type> {
	public:
		typedef typename T::value_type value_type;

		FillingIterator ( T parIterator, S parStart, S parStep );
		~FillingIterator ( void ) noexcept(noexcept(std::declval<T>().~T())) {}

	private:
		friend class boost::iterator_core_access;

		void increment ( void );
		bool equal ( const FillingIterator& parOther ) const;
		value_type dereference ( void ) const;

		T m_iterator;
		const S m_step;
		S m_pos;
	};

	template <typename T, typename S, typename ValGet, typename Upgrade>
	FillingIterator<T, S, ValGet, Upgrade>::FillingIterator (T parIterator, S parStart, S parStep) :
		m_iterator(parIterator),
		m_step(parStep),
		m_pos(parStart)
	{
	}

	template <typename T, typename S, typename ValGet, typename Upgrade>
	void FillingIterator<T, S, ValGet, Upgrade>::increment() {
		if (!(m_pos < ValGet()(*m_iterator))) {
			++m_iterator;
		}
		m_pos += m_step;
	}

	template <typename T, typename S, typename ValGet, typename Upgrade>
	bool FillingIterator<T, S, ValGet, Upgrade>::equal (const FillingIterator& parOther) const {
		return m_iterator == parOther.m_iterator && m_pos == parOther.m_pos;
	}

	template <typename T, typename S, typename ValGet, typename Upgrade>
	typename FillingIterator<T, S, ValGet, Upgrade>::value_type FillingIterator<T, S, ValGet, Upgrade>::dereference() const {
		if (ValGet()(*m_iterator) == m_pos) {
			return *m_iterator;
		}
		else {
			return Upgrade()(m_pos, *m_iterator);
		}
	}
} //namespace stats

#endif
