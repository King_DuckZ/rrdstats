#ifndef id967BD6EB64134FBB8010A3A933384A83_H
#define id967BD6EB64134FBB8010A3A933384A83_H

#include <string>
#include <vector>
#include <ctime>

namespace stats {
	class RRDWrapper {
	public:
		enum CreationMode {
			CreationMode_Overwrite,
			CreationMode_IgnoreExisting
		};

		explicit RRDWrapper ( const std::string& parFile );
		~RRDWrapper ( void ) noexcept;

		std::time_t last ( void ) const;
		std::time_t first ( void ) const;
		double version ( void ) const;
		void update ( const std::vector<std::string>& parArgs ) const;
		void create ( const std::vector<std::string>& parArgs, CreationMode parCreatMode ) const;
		void restore ( const std::vector<std::string>& parArgs ) const;
		void dump ( const std::vector<std::string>& parArgs ) const;
		void tune ( const std::vector<std::string>& parArgs ) const;
		void resize ( const std::vector<std::string>& parArgs ) const;

	private:
		const std::string m_file;
	};
} //namespace stats

#endif
