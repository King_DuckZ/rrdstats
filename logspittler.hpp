#ifndef idC3537FB6231D479E994F7CA3AFFF7096_H
#define idC3537FB6231D479E994F7CA3AFFF7096_H

#include "fillingiterator.hpp"
#include <string>
#include <memory>
#include <cstdint>
#include <ctime>
#include <cstddef>
#include <map>

namespace stats {
	class LogSpittler {
		struct TimeFromPair {
			std::time_t operator() (const std::pair<std::time_t, std::size_t>& parVal) { return parVal.first; }
		};
		struct TimeToPair {
			std::pair<std::time_t, std::size_t> operator() (std::time_t parTime, const std::pair<std::time_t, std::size_t>&) { return std::make_pair(parTime, static_cast<std::size_t>(0)); }
		};

	public:
		typedef FillingIterator<std::map<std::time_t, std::size_t>::const_iterator, std::time_t, TimeFromPair, TimeToPair> const_iterator;

		LogSpittler ( void );
		~LogSpittler ( void ) noexcept;

		void processLine ( const std::string& ) const;
		void printTimestampCounters ( void ) const;
		uint64_t oldestTimestamp ( void ) const;
		uint64_t newestTimestamp ( void ) const;
		void setInterval ( uint32_t parNewInterval );

		const_iterator hitsBegin ( void ) const;
		const_iterator hitsEnd ( void ) const;

	private:
		struct LocalData;
		std::unique_ptr<LocalData> m_localData;
		uint32_t m_interval;
	};

	std::string oldestTimestamp ( const LogSpittler& parSpittler );
} //namespace stats

#endif
