#include "rrdwrapper.hpp"
#include <rrd.h>
#include <cstddef>
#include <algorithm>
#include <stdexcept>
#include <memory>
#include <iostream>
#include <sstream>
#include <cassert>
#include <unistd.h>
#include <boost/lexical_cast.hpp>

extern "C" {
	int optind;
	int opterr;
}

namespace stats {
	namespace {
		void paramVectorToCParams (const std::vector<std::string>& parParams, std::vector<const char*>& parOut) {
			parOut.reserve(parOut.size() + parParams.size() + 1);

			for (const auto& currStr : parParams) {
				parOut.push_back(currStr.c_str());
			}
			parOut.push_back(nullptr);
		}

		template <typename R, typename F>
		R execAction (const char* parPrefix, const std::vector<std::string>& parParams, F parCallable) {
			if (parParams.empty())
				return 0;
			std::vector<const char*> params;
			if (parPrefix)
				params.push_back(parPrefix);
			paramVectorToCParams(parParams, params);
			char** nonconstParams = const_cast<char**>(params.data());

			optind = opterr = 0;
			rrd_clear_error();
			return parCallable(static_cast<int>(params.size() - 1), nonconstParams);
		}

		void throwIfRRDError (const char* parPrefix) {
			const int ret = rrd_test_error();
			if (ret) {
				std::ostringstream oss;
				oss << "rrdtool " << parPrefix << " returned " << ret << ": \"" << rrd_get_error() << "\"";
				throw std::runtime_error(oss.str());
			}
		}

		bool fileExists (const std::string& parFile) {
			//see: http://pubs.opengroup.org/onlinepubs/009695399/functions/access.html
			const int ret = access(parFile.c_str(), F_OK);
			return static_cast<bool>(ret == 0);
		}
	} //unnamed namespace

	RRDWrapper::RRDWrapper (const std::string& parFile) :
		m_file(parFile)
	{
	}

	RRDWrapper::~RRDWrapper() noexcept {
	}

	std::time_t RRDWrapper::last() const {
		const std::vector<std::string> pars {"last", m_file};
		const std::time_t ret = execAction<std::time_t>(nullptr, pars, rrd_last);
		throwIfRRDError("last");
		return ret;
	}

	std::time_t RRDWrapper::first() const {
		const std::vector<std::string> pars {"first", m_file};
		const std::time_t ret = execAction<std::time_t>(nullptr, pars, rrd_first);
		throwIfRRDError("first");
		return ret;
	}

	double RRDWrapper::version() const {
		const double ret = rrd_version();
		throwIfRRDError("version");
		return ret;
	}

	void RRDWrapper::update (const std::vector<std::string>& parArgs) const {
		execAction<int>("update", parArgs, rrd_update);
		throwIfRRDError("update");
	}

	void RRDWrapper::create (const std::vector<std::string>& parArgs, CreationMode parCreatMode) const {
		if (fileExists(m_file)) {
			if (parCreatMode == CreationMode_Overwrite) {
				const int removed = unlink(m_file.c_str());
				switch (removed) {
				case 0:
					break;
				case EACCES:
					throw std::runtime_error(std::string("Access denied while removing ") + m_file);
				case EBUSY:
					throw std::runtime_error(std::string("File ") + m_file + " can't be removed because it's in use by another process");
				default:
					throw std::runtime_error(std::string("An error occurred while removing ") + m_file + ": " + boost::lexical_cast<std::string>(removed));
				}
			}
			else {
				return;
			}
		}
		execAction<int>("create", parArgs, rrd_create);
		throwIfRRDError("create");
	}

	void RRDWrapper::restore (const std::vector<std::string>& parArgs) const {
		execAction<int>("restore", parArgs, rrd_create);
		throwIfRRDError("restore");
	}

	void RRDWrapper::dump (const std::vector<std::string>& parArgs) const {
		execAction<int>("dump", parArgs, rrd_create);
		throwIfRRDError("dump");
	}

	void RRDWrapper::tune (const std::vector<std::string>& parArgs) const {
		execAction<int>("tune", parArgs, rrd_create);
		throwIfRRDError("tune");
	}

	void RRDWrapper::resize (const std::vector<std::string>& parArgs) const {
		execAction<int>("resize", parArgs, rrd_create);
		throwIfRRDError("resize");
	}
} //namespace stats
