#include "logspittler.hpp"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/regex.hpp>
#include <map>
#include <cstddef>
#include <ctime>
#include <vector>
#include <locale>
#include <sstream>
#include <ciso646>
#include <iostream>

namespace stats {
	struct LogSpittler::LocalData {
		boost::basic_regex<char> lineRegex;
		std::map<std::time_t, std::size_t> hits;
		std::vector<std::locale> locales;
	};

	namespace {
		const uint32_t DefInterval = 60;

		std::time_t timeToTimet (const boost::posix_time::ptime& parPt) {
			boost::posix_time::ptime timetStart(boost::gregorian::date(1970, 1, 1));
			boost::posix_time::time_duration diff = parPt - timetStart;
			return diff.ticks() / boost::posix_time::time_duration::rep_type::ticks_per_second;
		}

		void increaseCounterOrAdd (std::map<std::time_t, std::size_t>& parMap, std::time_t parTimestamp) {
			const auto itFound = parMap.lower_bound(parTimestamp);
			if (parMap.end() != itFound and not (parMap.key_comp()(parTimestamp, itFound->first))) {
				++itFound->second;
			}
			else {
				parMap.insert(itFound, std::make_pair(parTimestamp, static_cast<size_t>(1)));
			}
		}
	} //unnamed namespace

	std::string oldestTimestamp (const LogSpittler& parSpittler) {
		std::ostringstream oss;
		oss << parSpittler.oldestTimestamp();
		return oss.str();
	}

	LogSpittler::LogSpittler() :
		m_localData(new LocalData),
		m_interval(DefInterval)
	{
		m_localData->lineRegex.assign("^\\S+ \\S+ \\S+ \\[([^\\]]+)\\].*$", boost::regex_constants::perl);
		m_localData->locales.push_back(std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%d/%b/%Y:%H:%M:%S %z")));
	}

	LogSpittler::~LogSpittler() noexcept {
	}

	void LogSpittler::setInterval (uint32_t parNewInterval) {
		m_interval = (parNewInterval > 0 ? parNewInterval : DefInterval);
	}

	void LogSpittler::processLine (const std::string& parLine) const {
		boost::smatch m;
		const bool matched = boost::regex_match(parLine.begin(), parLine.end(), m, m_localData->lineRegex);
		if (matched) {
			boost::posix_time::ptime pt;
			std::istringstream iss(m[1]);
			for (auto& loc : m_localData->locales) {
				iss.imbue(loc);
				iss >> pt;
				if (pt != boost::posix_time::ptime())
					break;
			}

			const std::time_t timestamp = timeToTimet(pt);
			increaseCounterOrAdd(m_localData->hits, timestamp - (timestamp % m_interval));
		}
	}

	void LogSpittler::printTimestampCounters() const {
		if (m_localData->hits.empty())
			return;

		struct TimestampedCount {
			TimestampedCount (std::time_t parTime, std::size_t parCount) : timestamp(parTime), count(parCount) {}
			bool operator< (const TimestampedCount& parOther) const noexcept { return timestamp < parOther.timestamp; }
			std::time_t timestamp;
			std::size_t count;
		};

		std::vector<TimestampedCount> sortedCounts;
		sortedCounts.reserve(m_localData->hits.size());
		for (const auto& itCurr : m_localData->hits) {
			sortedCounts.push_back(TimestampedCount(itCurr.first, itCurr.second));
		}
		std::sort(sortedCounts.begin(), sortedCounts.end());

		std::time_t lastTime = sortedCounts.begin()->timestamp;
		for (const auto& itCurr : sortedCounts) {
			if (itCurr.timestamp - lastTime > m_interval) {
				for (std::time_t t = lastTime; t < itCurr.timestamp; t += m_interval) {
					std::cout << "rrdtool update webhits.rrd " << t << ":0\n";
				}
			}
			lastTime = itCurr.timestamp + m_interval;

			std::cout << "rrdtool update webhits.rrd " << itCurr.timestamp << ":" << itCurr.count << "\n";
		}
	}

	uint64_t LogSpittler::oldestTimestamp() const {
		if (m_localData->hits.empty())
			return 0;

		return static_cast<uint64_t>(m_localData->hits.begin()->first);
	}

	uint64_t LogSpittler::newestTimestamp() const {
		if (m_localData->hits.empty())
			return 0;

		return static_cast<uint64_t>(m_localData->hits.rbegin()->first);
	}

	LogSpittler::const_iterator LogSpittler::hitsBegin() const {
		return const_iterator(m_localData->hits.begin(), static_cast<std::time_t>(oldestTimestamp()), static_cast<std::time_t>(m_interval));
	}

	LogSpittler::const_iterator LogSpittler::hitsEnd() const {
		return const_iterator(m_localData->hits.end(), static_cast<std::time_t>(newestTimestamp()) + m_interval, static_cast<std::time_t>(m_interval));
	}
} //namespace stats
