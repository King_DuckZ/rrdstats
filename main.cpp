#include "logspittler.hpp"
#include "rrdwrapper.hpp"
#include <iostream>
#include <string>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cstdint>
#include <boost/lexical_cast.hpp>

namespace {
	const uint32_t DefInterval = 60;

	class line {
		std::string data;
	public:
		friend std::istream &operator>>(std::istream &is, line &l) {
			std::getline(is, l.data);
			return is;
		}
		operator std::string() const { return data; }
	};

} //unnamed namespace

int main (int, char**) {
	stats::LogSpittler spittler;
	spittler.setInterval(DefInterval);

	std::for_each(
		std::istream_iterator<line>(std::cin),
		std::istream_iterator<line>(),
		std::bind(&stats::LogSpittler::processLine, &spittler, std::placeholders::_1)
	);

	//spittler.printTimestampCounters();

	std::vector<std::string> pars {
		"rrdtool",
		"create",
		"webhits.rrd",
		"--start",
		stats::oldestTimestamp(spittler),
		"-s",
		boost::lexical_cast<std::string>(DefInterval),
		std::string("DS:hits:GAUGE:") + boost::lexical_cast<std::string>(DefInterval * 2) + ":0:U",
		"RRA:AVERAGE:.5:5:600000",
		"RRA:AVERAGE:.5:30:602938",
		"RRA:AVERAGE:.5:60:301469",
		"RRA:AVERAGE:.5:240:75367",
		"RRA:AVERAGE:.5:1440:12561"
	};
	//stats::rrdCreate(pars);
	stats::RRDWrapper rrd("webhits.rrd");
	std::cout << "rrdlib version " << rrd.version() << "\n";
	std::cout << rrd.last() << "\n";
	std::cout << "oldest: " << spittler.oldestTimestamp() << "\n";
	std::cout << "newest: " << spittler.newestTimestamp() << "\n";

	return 0;
}
